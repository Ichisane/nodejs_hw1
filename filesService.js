//requires
const fs = require('fs');
const path = require("path");
//constants
const FILE_PATH = './files';
const FILE_EXTENSIONS = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];


function createFile(req, res) {
  const { filename, content } = req.body;
  const filePath = FILE_PATH + '/' + filename;

  if (!content) {
    return res.status(400).json({
      "message": "Please specify 'content' parameter"
    });
  }
try {
  fs.writeFile(`${filePath}`, `${content}`, (err) => {
    
    res.status(200).send({ "message": "File created successfully" });
  })
} catch (err) {
  return res.status(500).send({ "message": "Server Error" });
}
}

function getFiles(req, res, next) {
  // Your code to get all files.
  try {
    fs.readdir(FILE_PATH, function(err, files) {
      if (err) {
        return res.status(400).send({ "message": "Client Error" });
      }
      const fileNames = files.map(function(file) {
        return file;
      })
  
      return res.status(200).json({
        "message": "Success",
        "files": fileNames
      });
  
    })
  } catch(err) {
    return res.status(500).send({ "message": "Server Error" });
  }
}

const getFile = (req, res) => {
  try {
    fs.readFile(`${FILE_PATH}/${req.params.filename}`, 'utf8', (err, data) => {
      if (err) return res.status(400).json({
        message: `No file with ${req.params.filename} filename found`,
      });
      fs.stat(`${FILE_PATH}/${req.params.filename}`, (err, stat) => {
        if (err) throw err;
        res.status(200).json({
          "message": "Success",
          "filename": req.params.filename,
          "content": data,
          "extension": path.extname(req.params.filename),
          "uploadedDate": stat.birthtime
        });
      })
    });
  }
  catch(err) {
    return res.status(500).send({ "message": "Server Error" });
  }
}


// const updateFile = (req, res) => {
//       fs.appendFile(`${pathToFiles}/${req.params.filename}`, `${req.params.data}`,  (err) =>  {
//         if (err) throw err;
//         return res.status(200).json({
//           'message' : 'Good job, document updated'
//         })
//       });
// }

// const deleteFile = (req, res) => {
//       fs.unlink(`${pathToFiles}/${req.params.filename}`,  (err) => {
//         if (err) throw err;
//         return res.status(200).json({
//           'message' : 'File has been delete'
//         })
//     })
// }

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file
module.exports = {
  createFile,
  getFiles,
  getFile,
}